public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1, "Giang", "Ton", 15000);
        Employee employee2 = new Employee(2, "Ninh", "Tran", 20000);
        
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());

        int luongemployee1 = employee1.getAnnualSalary();
        int luongemployee2 = employee2.getAnnualSalary();
        System.out.println("Nhân viên thứ nhất " + employee1.getFirstName() + " " + employee1.getLastName() + " có lương 1 năm là "+ luongemployee1);
        System.out.println("Nhân viên thứ hai " + employee2.getFirstName() + " " + employee2.getLastName() + " có lương 1 năm là "+ luongemployee2);
        float luongtangemp1 = employee1.raiseSalary(10);
        float luongtangemp2 = employee2.raiseSalary(20);
        System.out.println("Lương nhân viên thứ nhất sau khi tăng là " + luongtangemp1);
        System.out.println("Lương nhân viên thứ hai sau khi tăng là " + luongtangemp2);
    }
}
