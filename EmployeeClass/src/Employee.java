public class Employee {
    private int id;
    private String firstName; 
    private String lastName; 
    private int salary; 
    public Employee(){
        super();
    }
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public int getAnnualSalary(){
        return salary*12;
    }
    public float raiseSalary(float percent){
        return ((percent/100)+1)*(this.salary);
    }
    @Override
    public String toString(){
        return String.format("Employee [id= " + id + ", name = " + firstName + " " +  lastName + " , salary " + salary +"]");
    }
}
